#/bin/env python3
import numpy as np

def fix42D(rows=1, array=[]):
    #total len
    lenA = len(array)
    #just create a bigger 2D matrix so that array fixes 
    rowSize = int(len(array)/rows) + 1
    matrixDiff = (rowSize * rows) - lenA
    #fill the spaces
    array = array.tolist()
    for i in range(matrixDiff): array.append(0)
    #convert again into a numpy array
    array = np.asarray(array)
    array = array.reshape(rows,rowSize)
    return array


if __name__ == "__main__":
    totalInvestment = 10000000
    ethPrice = 250
    days = 77
    DTSlst = np.random.randint(225, 300, int(totalInvestment/ethPrice))
    array = fix42D(days,DTSlst)
    print(array)
