# Price Mechanism

**A Jupyter Semi-Stable Token Modeling and Research paper in Lyx.**


## Table of Contents 

[[_TOC_]] 

## Price Mechanism Tree

```sh.
├── DECA-2.2.ipynb
├── DECA-Model-Basics.ipynb
├── DECA-PIE.ipynb
├── DECA_Price_Mechanism_Paper
│   ├── DECA-2.2-PublicationFixed.ipynb
│   ├── DECAFULL.png
│   ├── DecaMarketLowestFull.png
│   ├── DecaMarketLowest.png
│   ├── DecaPieChart.png
│   ├── DECA_Price_Mechanism.lyx
│   ├── DECA_Price_Mechanism.pdf
│   ├── DECATableShort.png
│   ├── GoldPrice.jpg
│   └── ICOChart.png
├── fix42D.py
├── GoldPrices.ipynb
├── historyPricesPerDay
│   └── GoldPrice-piketty.csv
├── LICENSE
├── README.md
└── requirements.txt
```

## Requirements
* python >= 3.8.2
* python3-dev == 3.8.2
* python3-setuptools == 45.2.0-1
* python3-pip == 20.0.2
* virtualenv == 20.0.17

## Instalation

**First you need to have the python base system requirements**
```sh
$ sudo apt-get install build-essential python3 python3-dev python3-setuptools python3-pip virtualenv
```
**clone the repo and switch to price-mechanism directory:**
```sh
$ git clone https://gitlab.com/deca-currency/price-mechanism.git
$ cd price-mechanism
```
**set the virtual enviroment**
```sh
$ virtualenv -p python3 myenv
$ source myenv/bin/activate
```

**install requirements.txt into the virtualenv:**
```sh
(myenv)$ pip3 install -r requirements.txt
```
**Run the Jupyter Notebook**
```sh 
$ jupyter-notebook
```
> NOTE:
> the server should be running by default at http://127.0.0.1:8888


## Notebooks Description


| Notebook | Description |
|---|---|
| ./DECA.ipynb  | Simulation for price mechanism model |
| ./DECA-Model-Basics.ipynb| Price Mechanism mathematical model|
| ./DECA_Price_Mechanism_Paper/DECA-PIE.ipynb  | Plan for DECA carbon credits backup |
| ./DECA_Price_Mechanism_Paper/GoldPrices.ipynb  | Gold prices chart |
| ./DECA_Price_Mechanism_Paper/GoldPrices.ipynb  | Simulation with fixed names for the paper |

## DECA Price Mechanism Paper 

**Requires LyX 2.3.4.2 to edit**

```sh
$ lyx ./DECA_Price_Mechanism_Paper/DECA_Price_Mechanism.lyx

```

***
## License

[**GPLV3**](./LICENSE).

## Information and contacts.

***Developers***

- David E. Perez Negron [david@neetsec.com](mailto:david@neetsec.com)
- Marco Blanke [marco@neetsec.com](mailto:marco@neetsec.com)

## ToDo

 - DEFI models
 - Collateral simulation